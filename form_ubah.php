<?php

session_start();

include 'koneksi.php';

$id = $_GET['id'];

$sql = "SELECT * FROM users WHERE id='$id'";
$result = $koneksi->query($sql);
$hasil = $result->fetch_assoc();

// untuk mencegah user langsung pergi ke home.php tanpa login
if ($_SESSION["login"] !== 1) {
    header("Location:index.php?pesan=login");
}

?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <style>
        @font-face {
            font-family: quicksand;
            src: url(Font/Quicksand-Medium.ttf);
        }

        * {
            font-family: quicksand;

        }
    </style>

    <title>Registrasi</title>
</head>

<body style="background-image: url(Img/wallpaper.jpg); background-size:cover;">
    <div class="container" style="width:510px; margin-top:150px;">

        <form class="card" style=" background:rgba(0,0,0,0.5); border-radius:20px;" action="proses_ubah.php" method="POST">
            <h2 class="mt-4" style="text-align:center; font-size:35px; font-weight:bold; color:white;">Ubah Data</h2>

            <input type="hidden" name="id" value="<?=$id?>">

            <div class="form-group mt-2 mr-3 ml-3">
                <label style="color: white;"> Nama </label>
                <input value="<?= $hasil['nama'];?>" name="nama" type="text" class="form-control" required>
            </div>

            <div class="form-row mr-2 ml-2">
                <div class="col">
                    <label style="color: white;"> Username</label> <br>
                    <input value="<?=  $hasil['username'];?>" name="username" type="text" class="form-control" required>
                </div>

                <div class="col mr-2 ml-2">
                    <label style="color: white;">Email </label><br>
                    <input value="<?= $hasil['email'];?>" name="email" type="email" class="form-control" required>
                </div>
            </div>

            <!-- <div class="form-row mr-2 ml-2 mt-2">
                <div class="col">
                    <label style="color: white;">Password</label><br>
                    <input  name="password" type="password" class="form-control" required>
                </div>

                <div class="col mr-2 ml-2">
                    <label style="color: white;">Konfirmasi password</label><br>
                    <input  name="konfirmasi_password" type="password" class="form-control" required>
                </div>
            </div> -->

            <button type="submit" class="btn btn-primary mt-3 mb-3 mr-3 ml-3">Ubah</button>

        </form>
    </div>


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>

</html>