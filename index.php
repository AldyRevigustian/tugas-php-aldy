<?php

// mengaktifkan session
session_start();

if (isset($_SESSION["login"]) && $_SESSION["login"] === 1) {
    header("Location:home.php");
}

// menampilkan pesan 
if (isset($_GET["pesan"])) {

    if ($_GET["pesan"] === "gagal_login") {
        $warna = "danger";
        $pesan = "Akun Tidak Di Temukan";
    }

    if ($_GET["pesan"] === "berhasil_registrasi") {
        $warna = "success";
        $pesan = "Registrasi Berhasil";
    }

    if ($_GET["pesan"] === "berhasil_logout") {
        $warna = "success";
        $pesan = "Anda Telah Logout";
    }

    if ($_GET["pesan"] === "login") {
        $warna = "danger";
        $pesan = "Silahkan Login Terlebih Dahulu";
    }
}

?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <style>
        @font-face {
            font-family: quicksand;
            src: url(Font/Quicksand-Medium.ttf);
        }

        * {
            font-family: quicksand;

        }
    </style>

    <title>Login</title>
</head>

<body style="background-image: url(Img/wallpaper2.jpg); background-size:cover;">
    <div class="container" style="width:510px; margin-top:60px;">

        <form class="card" style=" background:rgba(0,0,0,0.5); border-radius:20px;" action="proses_login.php" method="POST">
            <img src="/Img/pic2.png" style="width:150px;  margin:30px auto auto auto;">
            <h3 class="mt-4" style="text-align:center; font-weight:bold; font-size:35px; color:white;">Login</h3>
            <?php
            if (isset($pesan)) {

            ?>
                <div class="alert alert-<?= $warna; ?> mt-3 mr-3 ml-3" role="alert">
                    <span> <?php echo $pesan; ?> </span>
                </div>
            <?php
            }
            ?>

            <div class="form-group mt-2 mr-3 ml-3">
                <input name="username" type="text" class="form-control" placeholder="Username" required>
            </div>

            <div class="form-group mt-1 mr-3 ml-3">
                <input name="password" type="password" class="form-control" placeholder="Password" required>
            </div>

            <button type="submit" class="btn btn-primary mt-2 mb-3 mr-3 ml-3">Login</button>
            <span class="mt-2 mb-3 ml-3" style="color: white;">
                Belum Punya Akun?
                <a href="registrasi.php" style="color: #007BFF;"><i>Register<i></a>
            </span>
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>


</html>