<?php
// menampilkan pesan 
if (isset($_GET["pesan"])) {

    if ($_GET["pesan"] === "tidak_sama") {
        $warna = "danger";
        $pesan = "Password Tidak Sama";
    }
}
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <style>
        @font-face {
            font-family: quicksand;
            src: url(Font/Quicksand-Medium.ttf);
        }

        * {
            font-family: quicksand;

        }
    </style>

    <title>Registrasi</title>
</head>

<body style="background-image: url(Img/wallpaper2.jpg); background-size:cover;">
    <div class="container" style="width:510px; margin-top:90px;">

        <form class="card" style=" background:rgba(0,0,0,0.5); border-radius:20px;" action="proses_registrasi.php" method="POST">
            <h2 class="mt-5" style="text-align:center; font-size:35px; font-weight:bold; color:white;">Registrasi</h2>

            <?php
            if (isset($pesan)) {

            ?>
                <div class="alert alert-<?= $warna; ?> mt-3 mr-3 ml-3" role="alert">
                    <span style="font-style:bold;"> <?php echo $pesan; ?> </span>
                </div>
            <?php
            }
            ?>

            <div class="form-group mt-1 mr-3 ml-3">
                <label style="color: white;"> Nama </label>
                <input name="nama" type="text" class="form-control" required>
            </div>

            <div class="form-row mr-2 ml-2">
                <div class="col">
                    <label style="color: white;"> Username</label> <br>
                    <input name="username" type="text" class="form-control" required>
                </div>

                <div class="col mr-2 ml-2">
                    <label style="color: white;">Email </label><br>
                    <input name="email" type="email" class="form-control" required>
                </div>
            </div>

            <div class="form-row mr-2 ml-2 mt-2">
                <div class="col">
                    <label style="color: white;">Password</label><br>
                    <input name="password" type="password" class="form-control" required>
                </div>

                <div class="col mr-2 ml-2">
                    <label style="color: white;">Konfirmasi password</label><br>
                    <input name="konfirmasi_password" type="password" class="form-control" required>
                </div>
            </div>

            <button type="submit" class="btn btn-primary mt-3 mb-3 mr-3 ml-3">Register</button>

            <span class="mt-2 mb-3 ml-3" style="color: white;">
                Sudah Punya Akun?
                <a href="index.php"><i>Login<i></a>
            </span>
        </form>
    </div>


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>

</html>