<?php

// mengaktifkan session
session_start();

include 'koneksi.php';
$username = $_SESSION["username"];

$sql = "SELECT * FROM users WHERE username='$username'";
$result = $koneksi->query($sql);
$hasil = $result->fetch_assoc();

// untuk mencegah user langsung pergi ke home.php tanpa login
if ($_SESSION["login"] !== 1) {
    header("Location:index.php?pesan=login");
}

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <!-- Css Ku-->
    <style>
        @font-face {
            font-family: quicksand;
            src: url(Font/Quicksand-Medium.ttf);
        }

        * {
            font-family: quicksand;

        }

        .container {
            font-size: 15px;
        }


        .home-picture img {

            width: 100%;
            background-size: cover;
            background-position: center;
        }
    </style>
    <title>My Blog</title>
</head>

<body>

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand " href="home.php" style="font-weight: bold;">
                <i style="font-size: 23px; color: rgb(245, 245, 245);"></i> <?php echo $hasil["username"]; ?>
                Website</a>

            <div class="navbar-nav">
                <a class="nav-link active ml-3" style="font-size:18px; " href="home.php">Home </a>
                <a class="nav-link " style="font-size:18px; " href="akun.php">Akun <span class="sr-only">(current)</span></a>
                <a class="nav-link " style="font-size:18px; " href="admin.php">Admin</a>

            </div>

            <div class="ml-auto navbar-nav">
                <a type="button" style="width:110px;" class="btn btn-success " href="logout.php">Log out</a>
            </div>
        </div>
    </nav>
    <!-- Navbar End-->



    <!-- Home -->
    </div>
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="img/welcome.jpg" alt="First slide">
            </div>
        </div>
        <!-- Home End -->

        <!-- Footer -->
        <footer style="font-weight: 100;" class="bg-dark text-white">
            <div class="container">
                <div class="row pt-3">
                    <div class="col text-center">
                        <p> &copy; Copyright By <?php echo $hasil["nama"]; ?></p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>

</html>