<?php

// mengaktifkan session
session_start();

include 'koneksi.php';

$username = $_SESSION["username"];

$sql = "SELECT * FROM users";
$result = mysqli_query($koneksi, $sql);

$hasil = mysqli_fetch_all($result, MYSQLI_ASSOC);


// untuk mencegah user langsung pergi ke home.php tanpa login
if ($_SESSION["login"] !== 1) {
    header("Location:index.php?pesan=login");
}

if (isset($_GET["pesan"])) {

    if ($_GET["pesan"] === "gagal_hapus") {
        $warna = "danger";
        $pesan = "Gagal menghapus data";
    }

    if ($_GET["pesan"] === "berhasil_hapus") {
        $warna = "success";
        $pesan = "Berhasil menghapus data";
    }

    if ($_GET["pesan"] === "berhasil_ubah") {
        $warna = "success";
        $pesan = "Berhasil mengubah data";
    }
        if ($_GET["pesan"] === "gagal_ubah") {
        $warna = "danger";
        $pesan = "Gagal mengubah data";
    }

    if ($_GET["pesan"] === "berhasil_tambah") {
        $warna = "success";
        $pesan = "Berhasil tambah data";
    }

    if ($_GET["pesan"] === "gagal_tambah") {
        $warna = "success";
        $pesan = "Gagal tambah data";
    }


}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <!-- Css Ku-->
    <style>
        @font-face {
            font-family: quicksand;
            src: url(Font/Quicksand-Medium.ttf);
        }

        * {
            font-family: quicksand;

        }
    </style>

    <title>Admin</title>
</head>

<body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand " href="home.php" style="font-weight: bold;">
                <i style="font-size: 23px; color: rgb(245, 245, 245);"></i> <?php echo $_SESSION["username"]; ?>
                Website</a>

            <div class="navbar-nav">
                <a class="nav-link ml-3" style="font-size:18px; " href="home.php">Home </a>
                <a class="nav-link" style="font-size:18px; " href="akun.php">Akun <span class="sr-only">(current)</span></a>
                <a class="nav-link active" style="font-size:18px; " href="admin.php">Admin</a>

            </div>

            <div class="ml-auto navbar-nav">
                <a type="button" style="width:110px;" class="btn btn-success " href="logout.php">Log out</a>
            </div>
        </div>
    </nav>
    <!-- Navbar End-->


    <div class="container" style="margin-top: 100px; height:700px;">
        <h1><b>Data User</b></h1>
        <a class="btn btn-primary" href="form_tambah.php">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-plus-fill" viewBox="0 0 16 16">
                <path d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                <path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z" />
            </svg>
            Tambah Data
        </a>

        <?php
        if (isset($pesan)) {

        ?>
            <div class="alert alert-<?= $warna; ?> mt-3 " role="alert">
                <span> <?php echo $pesan; ?> </span>
            </div>
        <?php
        }
        ?>


        <table class="table table-bordered mt-3">
            <thead>
                <tr style="text-align: center;">
                    <th> No </th>
                    <th> Nama </th>
                    <th> Username </th>
                    <th> Email </th>
                    <th> Avatar</th>
                    <th> Action</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($hasil as $key => $user_data) {

                ?>
                    <tr>
                        <td><?= $key + 1 ?></td>
                        <td><?= $user_data["nama"]; ?></td>
                        <td><?= $user_data["username"]; ?></td>
                        <td><?= $user_data["email"]; ?></td>
                        <td><?= $user_data["avatar"]; ?></td>
                        <td style="text-align: center;">
                            <a style="width:100px;" class="btn btn-success" href="form_ubah.php?id=<?= $user_data['id']; ?>"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                    <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z" />
                                </svg> Ubah</a>
                            <a style="width:100px;" class="btn btn-danger" href="proses_hapus.php?id=<?= $user_data['id']; ?>"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
                                    <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z" />
                                </svg> Hapus</a>
                        </td>
                    </tr>
                <?php
                }
                ?>


            </tbody>
        </table>
    </div>






    <!-- Footer -->
    <footer style="font-weight: 100;" class="sticky-bottom bg-dark text-white">
        <div class="container">
            <div class="row pt-3">
                <div class="col text-center">
                    <p> &copy; Copyright By <?php echo $_SESSION["nama"]; ?></p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer End -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>

</html>