<?php

// mengaktifkan session
session_start();

// memasukan file code lain 
include 'koneksi.php';

// mengambil data dari form
$username = $_POST["username"];
$password = $_POST["password"];

// mengambil data dari database
$sql = "SELECT * FROM users WHERE username='$username'";
$result = $koneksi->query($sql);
$hasil = $result->fetch_assoc();


if ($result->num_rows > 0) {
    if (password_verify($password, $hasil["password"])) {
        $_SESSION["username"] = $hasil["username"];
        $_SESSION["login"] = 1;
        $_SESSION["nama"] = $hasil["nama"];
        $_SESSION["email"] = $hasil["email"];

        // redirect dan menampikan pesan
        header("Location: home.php?pesan=Berhasil_login");
    }
} else {
    // redirect dan menampikan pesan
    header("Location: index.php?pesan=gagal_login");
}
$koneksi->close();
